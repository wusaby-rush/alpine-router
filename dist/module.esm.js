// packages/alpn-router/src/index.js
window.onpushstate = new Event("onpushstate");
var routerOutlet = async (el, {expression}, {evaluate}) => {
  const routes = evaluate(expression);
  const handleLocation = async () => {
    const {pathname} = window.location;
    const page = routes[pathname] || routes["404"];
    const html = await fetch(page).then((data) => data.text());
    el.innerHTML = html;
  };
  window.addEventListener("onpushstate", handleLocation);
  window.onpopstate = handleLocation;
  window.onload = handleLocation;
};
var router = (el, {}, {cleanup}) => {
  let handler = (event) => {
    event = event || window.event;
    event.preventDefault();
    window.history.pushState({}, "", el.href);
    window.dispatchEvent(onpushstate);
  };
  el.addEventListener("click", handler);
  cleanup(() => {
    el.removeEventListener("click", handler);
  });
};
function src_default(Alpine) {
  Alpine.directive("router-outlet", routerOutlet);
  Alpine.directive("router", router);
}

// packages/alpn-router/builds/module.js
var module_default = src_default;
export {
  module_default as default
};
