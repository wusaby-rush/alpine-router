// this file should be a standalone module when finished
// TODO: add support for `x-router.back`
// TODO: add magic $url to get url variables

window.onpushstate = new Event("onpushstate");

const routerOutlet = async (el, { expression }, { evaluate }) => {
  const routes = evaluate(expression);
  
  const handleLocation = async () => {
    const { pathname } = window.location;
    const page = routes[pathname] || routes["404"];
    const html = await fetch(page).then(data => data.text());
    el.innerHTML = html;
  }

  window.addEventListener('onpushstate', handleLocation);
  window.onpopstate = handleLocation;
  window.onload = handleLocation;
}

const router = (el, {}, { cleanup }) => {
  let handler = (event) => {
      event = event || window.event;
      event.preventDefault();
      window.history.pushState({}, "", el.href);
      window.dispatchEvent(onpushstate);
  }
  el.addEventListener('click', handler);

  cleanup(() => {
      el.removeEventListener('click', handler)
  })
}

export default function (Alpine) {
  Alpine.directive('router-outlet', routerOutlet)
  Alpine.directive('router', router)
}
