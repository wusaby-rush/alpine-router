// this file should be a standalone module when finished
// TODO: add support for `v-router.back`
// TODO: add magic $url to get url variables

const routes = {
    '/': '/views/home.html',
    '/settings/': '/views/settings.html',
    '/details': '/views/details.html',
    '/inventory': '/views/inventory.html',
    '/settings/contact': '/views/contact.html',
    '/settings/currency': '/views/currency.html',
}

const root = document.querySelector('#app')
// get location of current tab
// determine matched page from routes
// fetch page
// inject root with fetched content
const handleLocation = async () => {
    const { pathname } = window.location
    const view = routes[pathname] || routes["404"]
    const html = await fetch(view).then(data => data.text())
    root.innerHTML = html
}

// handle location when page loaded
window.addEventListener('load', handleLocation)
// handle loaction when move inside history
window.addEventListener('popstate', handleLocation)

// handle location when anchor with v-router clicked
document.addEventListener('click', (e) => {
    let link = e.target.closest('[v-router]') || e.target.closest('[v-router-back]') || e.target
    if(link.matches('[v-router]')) {
        e.preventDefault()
        window.history.pushState({}, "", link.href)
        handleLocation()
    } else if  (link.matches('[v-router-back]')) {
        e.preventDefault()
        window.history.back()
    }
})
