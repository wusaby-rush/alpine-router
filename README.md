# alpn-router

is a lightweight router library for alpinejs.

## Usage

```js
import './style.css'

import Alpine from 'alpinejs'
import router from 'alpn-router'

// initilaze alpine
Alpine.plugin(router);
window.Alpine = Alpine;
Alpine.start()
```

## build library

1. clone `alpinjs` project
2. add `alpn-router` directory to `alpinejs/packages` directory
3. add `alpn-router` to `scripts/build.js` 
4. run `npm run build`
